program SincroBD;

uses
  Vcl.Forms,
  uMain in 'uMain.pas' {frmMain},
  Vcl.Themes,
  Vcl.Styles;

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := false;
  TStyleManager.TrySetStyle('Windows10 SlateGray');
  Application.Title := 'Sincronizacion de ArGestión';
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
