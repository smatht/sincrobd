unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Imaging.pngimage,
  Data.DB, Data.Win.ADODB, Vcl.Grids, Vcl.ComCtrls, Vcl.StdCtrls,
  System.Win.Registry, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, System.strutils, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FireDAC.Phys.PG, FireDAC.Phys.PGDef, Vcl.DBCtrls, Generics.Collections, Vcl.WinXPickers;

type
  TfrmMain = class(TForm)
    TrayIcon: TTrayIcon;
    img1: TImage;
    acnConnectionLocal: TADOConnection;
    pnlAbajo: TPanel;
    PageControl1: TPageControl;
    ts1: TTabSheet;
    ts2: TTabSheet;
    btnSincronizar: TButton;
    mmoLog: TMemo;
    shpLocal: TShape;
    shpRemoto: TShape;
    tsSentencias: TTabSheet;
    mmoSentencias: TMemo;
    lstTablas: TListBox;
    pbGeneral: TProgressBar;
    pbTabla: TProgressBar;
    fdConnectionRemoto: TFDConnection;
    fdqryRemoto: TFDQuery;
    fdcmnd: TFDCommand;
    btnSincroSeleccion: TButton;
    tblLocal: TADOQuery;
    btnSincronizarDesdeS: TButton;
    TabSheet1: TTabSheet;
    Label1: TLabel;
    chkConCambios: TCheckBox;
    adsEmpresas: TADOQuery;
    dsEmpresas: TDataSource;
    adsEmpresasidEmpresa: TSmallintField;
    adsEmpresassNombre: TStringField;
    adsEmpresassRazonSocial: TStringField;
    adsEmpresassDireccion: TStringField;
    adsEmpresasbActiva: TBooleanField;
    adsEmpresassTelefono: TStringField;
    adsEmpresassBase: TStringField;
    dblklstEmpresas: TDBLookupListBox;
    btnDescargarPedidos: TButton;
    acmdLocal: TADOCommand;
    tmrAutoExec: TTimer;
    tmrCerrar: TTimer;
    chkDesdeFecha: TCheckBox;
    dteDesde: TDateTimePicker;
    tmDesde: TTimePicker;
    Label2: TLabel;
    procedure ConectarLocal;
    procedure LimpiarProgressBar;
    function ActualizarRegistroRemoto(sTabla: string; MemTable: TADOQuery; var sErrMsg: string): boolean;
    function DescargarTabla(sTabla: string): boolean;
    function AgregarRegistroRemoto(sTabla: string; MemTable: TADOQuery; var sErrMsg: string): boolean;
    function AgregarRegistroLocal(sTabla: string; MemTable: TFDQuery; var sErrMsg: string): boolean;
    function Sincronizar(sTabla: string): boolean;
    procedure btnSincronizarClick(Sender: TObject);
    procedure acnConnectionLocalAfterConnect(Sender: TObject);
    procedure fdConnectionRemotoDisconnect(Connection: TADOConnection; var EventStatus: TEventStatus);
    function DBConexion: string;
    function DepositoVendedor(idVendedor:Integer): integer;
    function NumeroDePedido(sPedidoID:string): integer;
    function SucursalVendedor(idVendedor:Integer): integer;
    function MaxValorTabla(asCampo, asTabla, asCondicion: string): integer;
    function PuntoVentaVendedor(idVendedor:Integer): integer;
    procedure acnConnectionLocalDisconnect(Connection: TADOConnection; var EventStatus: TEventStatus);
    procedure btnSincroSeleccionClick(Sender: TObject);
    procedure fdConnectionRemotoAfterConnect(Sender: TObject);
    procedure btnSincronizarDesdeSClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dsEmpresasDataChange(Sender: TObject; Field: TField);
    procedure btnDescargarPedidosClick(Sender: TObject);
    procedure tmrAutoExecTimer(Sender: TObject);
    function NoCoincideRemotoConLocal(timeStamp: String): boolean;
    procedure tmrCerrarTimer(Sender: TObject);
    procedure lstTablasClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    function ExisteModelo(sTabla: string): boolean;
    procedure chkDesdeFechaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

const
  cRegKey: string = '\Software\SolucionesPalm\SP-Gestion';
  sModo: string = ''; //modo='A' para migrar datos de la bd de abogados

var
  frmMain: TfrmMain;
  gidEmpresa: integer;
  gStrConn,gsDBAdmin,gsDBPath, gsEmpresa:string;

implementation

{$R *.dfm}

function DBConexion: string;
var
	NomBase, PathBase: string;
begin
  with TRegistry.Create do
  try
    try
      Result := 'ERROR';
      try
          RootKey := HKEY_CURRENT_USER;
          if KeyExists(cRegKey) then
          begin
              if not OpenKeyReadOnly(cRegKey) then raise Exception.Create('Error al Abrir HKCU');
              try
                gStrConn := ReadString('StrConn');
              except
                gStrConn:='';
              end;
              if Trim(gStrConn)='' then Exception.Create('');
              NomBase := ReadString('NomBase');
              PathBase := ReadString('PathBase');
              gsDBAdmin := NomBase; //Este es el nombre de la base de datos de administracion multiempresa
              gsDBAdmin := PathBase; //Este es el path de la base de datos de administracion multiempresa
              Result := Format(gStrConn,[PathBase,NomBase]);
          end else raise Exception.Create('Error al Abrir HKCU');
      except
           RootKey := HKEY_LOCAL_MACHINE;
          if not OpenKeyReadOnly(cRegKey) then raise Exception.Create('Error al Abrir HKLM');
          gStrConn := ReadString('StrConn');
          NomBase := ReadString('NomBase');
          PathBase := ReadString('PathBase');
          gsDBAdmin := NomBase; //Este es el nombre de la base de datos de administracion multiempresa
          gsDBPath := PathBase; //Este es el path de la base de datos de administracion multiempresa
          Result := Format(gStrConn,[PathBase,NomBase]);
          if Trim(Result)='' then raise Exception.Create('Cadena de conexion no valida');
      end;
    except
      on E:Exception do
	      Result := 'ERROR';
    end;//try-e
  finally
    free;
  end;//try //with
end;

function DateTimeToISO(aValor: tDateTime): string;
var
  sDate, sTime: string;
begin
  sDate := FormatDateTime('yyyy-mm-dd', aValor);
  sTime := FormatDateTime('hh:nn:ss.zzz', aValor);
  Result := sDate + 'T' + sTime;
end;

function DateTimeToStrISO(aValor: tDateTime): string;
begin
  Result := QuotedStr(DateTimeToISO(aValor));
end;

function FloattoStrDec(aValor: Real): string;
var
  sTmp: string;
  FormatSettings: TFormatSettings;
begin
  GetLocaleFormatSettings(GetThreadLocale, FormatSettings);
  sTmp := FormatFloat('0.000#############', aValor);
  sTmp := StringReplace(sTmp, FormatSettings.ThousandSeparator, '', [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(sTmp, FormatSettings.DecimalSeparator, '.', [rfReplaceAll, rfIgnoreCase]);
end;


// +-----------------------------------------------+
// | Busco el mayor valor de un campo de una tabla |****************************
// +-----------------------------------------------+
function TfrmMain.MaxValorTabla(asCampo, asTabla, asCondicion: string): integer;
var
  adsMax: TADODataSet;
  sQuery: string;
begin
  adsMax := TADODataSet.Create(nil);
  try//finally
    with adsMax do
    begin
      Connection := acnConnectionLocal;
      //sQuery := Format('SELECT max(%s) FROM %s ',[asCampo, asTabla]);

      if Length(asCondicion) > 0 then
        asCondicion := 'WHERE ' + asCondicion;

      sQuery := Format('SELECT max(%s) FROM %s %s',[asCampo, asTabla, asCondicion]);

      CommandText := sQuery;
      Open;
      if not IsEmpty then
      	Result := Fields.Fields[0].AsInteger
      else
        Result := 0;
    end;//with
  finally
    adsMax.Free;
  end;//try
end;

// +-----------------------------------------------+
// | Busco si existe Modelo                         |****************************
// +-----------------------------------------------+
function TfrmMain.ExisteModelo(sTabla: string): boolean;
var
  adsMax: TADODataSet;
  sQuery: string;
begin
  adsMax := TADODataSet.Create(nil);
  try//finally
    with adsMax do
    begin
      Connection := acnConnectionLocal;

      sQuery := Format('SELECT 1 FROM Modelos where LOWER(idTabla)=LOWER(%s)',[QuotedStr(sTabla)]);

      CommandText := sQuery;
      Open;
      if not IsEmpty then
      	Result := true
      else
        Result := false;
    end;//with
  finally
    adsMax.Free;
  end;//try
end;

// +-----------------------------------------------+
// | Busco el punto de venta para el vendedor      |****************************
// +-----------------------------------------------+
procedure TfrmMain.PageControl1Change(Sender: TObject);
begin
  tmrCerrar.enabled := false;
  tmrAutoExec.enabled := false;
end;

function TfrmMain.PuntoVentaVendedor(idVendedor:Integer): integer;
var
  adsAux: TADODataSet;
  sQuery: string;
begin
  //punto de venta por defecto
  Result:=1;

  adsAux := TADODataSet.Create(nil);
  try//finally
    with adsAux do
    begin
      Connection := acnConnectionLocal;
      sQuery := 'SELECT idPV FROM Vendedores where idVendedor='+IntToStr(idVendedor);
      CommandText := sQuery;
      Open;
      if not IsEmpty then
      	Result := Fields.Fields[0].AsInteger
      else
        Result := 1;
    end;//with
  finally
    adsAux.Free;
  end;//try
end;

// +-----------------------------------------------+
// | Busco la Sucursal para el vendedor      |****************************
// +-----------------------------------------------+
function TfrmMain.SucursalVendedor(idVendedor:Integer): integer;
var
  adsAux: TADODataSet;
  sQuery: string;
begin
  //sucursal por defecto
  Result:=1;

  adsAux := TADODataSet.Create(nil);
  try//finally
    with adsAux do
    begin
      Connection := acnConnectionLocal;
      sQuery := 'SELECT idSucursal FROM Vendedores where idVendedor='+IntToStr(idVendedor);
      CommandText := sQuery;
      Open;
      if not IsEmpty then
      	Result := Fields.Fields[0].AsInteger
      else
        Result := 1;
    end;//with
  finally
    adsAux.Free;
  end;//try
end;


procedure TfrmMain.tmrAutoExecTimer(Sender: TObject);
begin
  tmrAutoExec.enabled := False;
  tmrAutoExec.Interval := 1800000;
  if btnSincronizar.Enabled = True then
  begin
    btnSincronizarClick(self);
  end;
end;

procedure TfrmMain.tmrCerrarTimer(Sender: TObject);
begin
  Close;
end;

function TfrmMain.DepositoVendedor(idVendedor:Integer): integer;
var
  adsAux: TADODataSet;
  sQuery: string;
begin
  //punto de venta por defecto
  Result:=1;

  adsAux := TADODataSet.Create(nil);
  try//finally
    with adsAux do
    begin
      Connection := acnConnectionLocal;
      sQuery := 'SELECT idDeposito FROM Vendedores where idVendedor='+IntToStr(idVendedor);
      CommandText := sQuery;
      Open;
      if not IsEmpty then
      	Result := Fields.Fields[0].AsInteger
      else
        Result := 1;
    end;//with
  finally
    adsAux.Free;
  end;//try
end;


function TfrmMain.NumeroDePedido(sPedidoID:string): integer;
var
  adsAux: TADODataSet;
  sQuery: string;
begin
  //punto de venta por defecto
  Result:=1;

  adsAux := TADODataSet.Create(nil);
  try//finally
    with adsAux do
    begin
      Connection := acnConnectionLocal;
      sQuery := 'SELECT idPedido FROM Pedidos where id='+QuotedStr(sPedidoID);
      CommandText := sQuery;
      Open;
      if not IsEmpty then
      	Result := Fields.Fields[0].AsInteger
      else
        Result := -1;
    end;//with
  finally
    adsAux.Free;
  end;//try
end;

procedure TfrmMain.acnConnectionLocalAfterConnect(Sender: TObject);
begin
  shpLocal.Brush.Color := clGreen;
  mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Conectado a las BD local.');
end;

procedure TfrmMain.acnConnectionLocalDisconnect(Connection: TADOConnection; var EventStatus: TEventStatus);
begin
  if Assigned(shpLocal) then
    shpLocal.Brush.Color := clWhite;
end;

procedure TfrmMain.fdConnectionRemotoAfterConnect(Sender: TObject);
begin
  shpRemoto.Brush.Color := clGreen;
  mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Conectado a las BD remota.');
end;

procedure TfrmMain.fdConnectionRemotoDisconnect(Connection: TADOConnection; var EventStatus: TEventStatus);
begin
  if Assigned(shpRemoto) then
    shpRemoto.Brush.Color := clWhite;
end;

procedure TfrmMain.FormShow(Sender: TObject);
var
  i: Integer;
  idEmpresaSeleccionada: integer;
begin
  gStrConn:=DBConexion;
  if  (gStrConn='error') then
       ShowMessage('No se puede cargar configuracion de la base de datos');

  if ParamCount > 1 then
    idEmpresaSeleccionada := StrToInt(ParamStr(2))
  else
    idEmpresaSeleccionada := -1;

  adsEmpresas.Close;
  adsEmpresas.ConnectionString:=StringReplace(gStrConn,'%s%s','admin', [rfIgnoreCase,rfReplaceAll]);
  try
       adsEmpresas.Open;
       adsEmpresas.First;
       gidEmpresa:= adsEmpresas.FieldByName('idEmpresa').AsInteger;
        if not adsEmpresas.IsEmpty then
        begin
            if idEmpresaSeleccionada <> -1 then
            begin
              while not adsEmpresas.eof do
              begin
                if adsEmpresas.FieldByName('idEmpresa').AsInteger = idEmpresaSeleccionada then
                begin
                  gidEmpresa:= adsEmpresas.FieldByName('idEmpresa').AsInteger;
                  break;
                end;
                adsEmpresas.Next;
              end;
            end
        end
        else
            gidEmpresa := -1

  except on e:exception do
  begin

      ShowMessage('No se puede identificar la empresa. '+e.Message);
  end;
  end;
  if ParamCount > 0 then
    if AnsiCompareStr(AnsiUpperCase(ParamStr(1)), 'AUTO') = 0 then
    begin
      tmrAutoExec.Interval := 3000;
      tmrAutoExec.Enabled := true;
    end;

  dteDesde.Date := trunc(date);
end;

function TfrmMain.DBConexion: string;
var
  sAux: string;
begin
  with TRegistry.Create do
  begin
    try
      try
        RootKey := HKEY_CURRENT_USER;
        sAux := cRegKey; // + '\ConfigBase';
        if OpenKeyReadOnly(sAux) then
        begin
          Result := ReadString('StrConn');
        end;
      except
        on E: Exception do
        begin
          Result := 'ERROR';
          ShowMessage('Error leyendo cadena de conexi�n.'#13#10 + E.Message);
        end; //on
      end; //try-e

      if (Result = '') or (Result = 'ERROR') then
      begin
        try
          RootKey := HKEY_LOCAL_MACHINE;
          sAux := cRegKey; // + '\ConfigBase';
          if OpenKeyReadOnly(sAux) then
          begin
            Result := ReadString('StrConn');
          end;
        except
          on E: Exception do
          begin
            Result := 'ERROR';
            ShowMessage('Error leyendo cadena de conexi�n.'#13#10 + E.Message);
          end; //on
        end; //try-e
      end;
    finally
      Free;
    end;
  end; //try //with
end;

procedure TfrmMain.dsEmpresasDataChange(Sender: TObject; Field: TField);
begin
    if not adsEmpresas.IsEmpty then
    begin
        gidEmpresa:=adsEmpresas.FieldByName('idEmpresa').AsInteger ;
        gsEmpresa:=adsEmpresas.FieldByName('sBase').AsString;
    end
    else
        gidEmpresa := -1
end;

procedure TfrmMain.ConectarLocal;
var
  sStrAuth, sPath: string;
begin
  acnConnectionLocal.Close;
  acnConnectionLocal.ConnectionString:=StringReplace(gStrConn,'%s%s',gsEmpresa, [rfIgnoreCase,rfReplaceAll]);
  acnConnectionLocal.Open;
end;

procedure TfrmMain.LimpiarProgressBar;
begin
  pbGeneral.Position := 0;
  pbTabla.Position := 0;
  pbGeneral.Max := lstTablas.Count;
end;


procedure TfrmMain.lstTablasClick(Sender: TObject);
begin
  tmrCerrar.enabled := false;
end;

function TfrmMain.AgregarRegistroLocal(sTabla: string; MemTable: TFDQuery; var sErrMsg: string): boolean;
var
  sQuery, sValor, sValorClave, sCampos, sValores: string;
  i, idPedido,idPV,idDeposito, V: integer;
begin
  if not MemTable.IsEmpty then
  begin
    try //except
      sQuery:='';
      with MemTable do
      begin
        V:=MemTable.FieldByName('idVendedor').AsInteger;
        if UpperCase(sTabla)='PEDIDOS' then
        begin
            idPedido:=MaxValorTabla('idPedido','Pedidos','idVendedor='+IntToStr(v)+' AND iOrigen=1')+1;

            idPV := PuntoVentaVendedor(V);

            sQuery:='INSERT INTO PEDIDOS '+
						  	'( id, idPedido, idVendedor, iOrigen, idCliente, sObserv, fPedido, '+
						  	'  fEntrega, sOperador, rTotBruto, idLotePedido, idSucursal, '+
						  	'  idCondVenta, idCondPago, idPV, idMoneda,rTotNeto, rTotDescuentos, '+
                     '  rDescuento, iCantCuotas, iPlazoCuota1, iPlazoCuotaN, iTipoPedido) '+
						  	'VALUES ( '+
                QuotedStr(MemTable.FieldByName('id').AsString) +', ' +
						  	IntToStr(idPedido)+' , '+
                IntToStr(V)+' , '+
                ' 1, '+     //Origen: Palm   (0=PC ; 1=Palm)
						  	MemTable.FieldByName('idCliente').AsString+' , '+
						  	QuotedStr(MemTable.FieldByName('sObservaciones').AsString)+' , '+
						  	DateTimeToStrISO(MemTable.FieldByName('fPedido').AsDateTime)+', '+ //FloattoStrDec(rF1)+' , '+
						  	DateTimeToStrISO(MemTable.FieldByName('fEntrega').AsDateTime)+', '+ //FloattoStrDec(rF2)+' , '+
						  	QuotedStr('SincronizAR')+' , '+
						  	FloattoStrDec(MemTable.FieldByName('TotalPedido').AsFloat)+' , '+
							  IntToStr(1)+', '+  //Lote
							  IntToStr(1)+', '+  //Sucursal
							  QuotedStr(MemTable.FieldByName('idCondVenta').AsString)+', '+ //CDSPed.FieldByName('idCondVenta').AsString)+' , '+
						   	QuotedStr(MemTable.FieldByName('idCondPago').AsString)+' , '+
						   	IntToStr(idPV)+', '+    //Punto de venta
						   	MemTable.FieldByName('idMoneda').AsString+', '+
                FloattoStrDec(MemTable.FieldByName('TotalNeto').AsFloat)+', '+
                FloattoStrDec(MemTable.FieldByName('rTotDescuentos').AsFloat)+', '+
                FloattoStrDec(MemTable.FieldByName('rDescuento').AsFloat)+', '+
                     IntToStr(1)+', '+
                     IntToStr(1)+', '+
                     IntToStr(1)+', '+
                     IntToStr(0)+' ) ';

        end;

        //Cargar PedidoItem

         if UpperCase(sTabla)='PEDIDOSITEMS' then
         begin
           idDeposito :=  DepositoVendedor(V);
           idPedido := NumeroDePedido(MemTable.FieldByName('PedidoID').AsString);
            if idPedido=-1  then
              raise Exception.Create('No se puede detectar idPedido del pedidoid+'+MemTable.FieldByName('PedidoID').AsString);

            //'(('+CDSItem.FieldByName('nCantidad').AsString+' * '+CDSItem.FieldByName('fPrecio').AsString+')+'+CDSItem.FieldByName('rSubDescuento').AsString+'), '+
           sQuery := 'INSERT INTO PEDIDOSITEMS '+
            '( id, idPedido, idVendedor, iOrigen, idNumLinea, iCantidad, '+
            '  rPrecio, idDeposito, idLote, idLista, idProducto, idUnidad, '+
                   '  bFacturado, bPendiente, bSinLote, iCantSub, rDescuento,rTotBruto,iCantUMxUnidad,cCostoUni ) '+
             'VALUES ( '+
                  QuotedStr(MemTable.FieldByName('id').AsString) +', ' +
                  IntToStr(idPedido)+', '+  //CDS.FieldByName('idPedido').AsString+' , '+
                  IntToStr(V)+', '+
                  ' 1, '+       //iOrigen
                  IntToStr(MemTable.FieldByName('idLinea').AsInteger)+', '+  //numlinea,
                  FloattoStrDec(MemTable.FieldByName('nCantidad').AsFloat)+', '+
                  FloattoStrDec(MemTable.FieldByName('fPrecio').AsFloat)+', '+
                  IntToStr(idDeposito)+' , '+  //deposito
                  ' NULL , '+  //lote  autodeteccion
                  MemTable.FieldByName('idLista').AsString+' , '+
                  MemTable.FieldByName('idProducto').AsString+' , '+
                  MemTable.FieldByName('idUnidad').AsString+' , '+
                  ' 0, 1, 0, 0,'+
                  FloattoStrDec(MemTable.FieldByName('rSubDescuento').AsFloat)+', '+
                  //'(('+CDSItem.FieldByName('nCantidad').AsString+' * '+CDSItem.FieldByName('fPrecio').AsString+')+'+CDSItem.FieldByName('rSubDescuento').AsString+'), '+
                  '(('+FloattoStrDec(MemTable.FieldByName('fPrecio').AsFloat)+' * '+FloattoStrDec(MemTable.FieldByName('nCantidad').AsFloat)+') * (1+('+FloattoStrDec(MemTable.FieldByName('rSubDescuento').AsFloat)+') / 100)), '+  // ((Pr * Q) * (1 + (D%) / 100))
                  FloattoStrDec(1)+', '+
                  FloattoStrDec(MemTable.FieldByName('cCostoUni').AsFloat)+' )';
         end;

         if UpperCase(sTabla)='PEDIDOSITEMSDESCUENTOS' then
         begin
           idPedido := NumeroDePedido(MemTable.FieldByName('PedidoID').AsString);
           if idPedido=-1  then
              raise Exception.Create('No se puede detectar idPedido del pedidoid+'+MemTable.FieldByName('PedidoID').AsString);
           sQuery := 'INSERT INTO PedidosItemsDescuentos '+
               '( id, idPedido, idVendedor, iOrigen, idNumLinea, idDescuento, '+
                     '  rValorDescuento, rMonto) '+
                     'VALUES ( '+
                            QuotedStr(MemTable.FieldByName('id').AsString) +', ' +
                            IntToStr(idPedido)+', '+
                            IntToStr(V)+', '+
                            ' 1, '+       //iOrigen
                            MemTable.FieldByName('idLinea').AsString+', '+  //la linea ahora viene con el xml
                            MemTable.FieldByName('idDescuento').AsString+', '+
                            FloattoStrDec(MemTable.FieldByName('rValorDescuento').AsFloat)+', '+
                            FloattoStrDec(MemTable.FieldByName('rMonto').AsFloat)+' )';
         end;

         if sQuery<>'' then
         begin
            mmoSentencias.Lines.Add(sQuery + ';');
            try
              acmdLocal.CommandText:=sQuery;
              acmdLocal.Execute;
              result := true;
            except
              on e: exception do
              begin
                mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Error al Insertar registro remoto ' + sTabla + ': ' + e.Message);
              end;
            end;

         end;
      end;
    except
      on e: exception do
      begin
        Result := false;
        sErrMsg := e.Message;
             //DebugLog(LocalPOS.Usuario, 'ActualizarRegistroLocal/'+sTabla+'/'+sCampoClave, 'ERROR ' + sErrMsg);
             //RegistrarDebug('No se puede Actualizar registro local error:'+sErrMsg);
      end;

    end;
  end;
end;

function TfrmMain.AgregarRegistroRemoto(sTabla: string; MemTable: TADOQuery; var sErrMsg: string): boolean;
var
  sQuery, sValor, sValorClave, sCampos, sValores: string;
  i: integer;
begin
  if not MemTable.IsEmpty then
  begin
    try //except
      with MemTable do
      begin
        sCampos := '';
        sValores := '';
        sQuery := 'INSERT INTO "' + sTabla + '" ';
        for i := 0 to Fields.Count - 1 do
        begin
          if fdqryRemoto.FindField(MemTable.Fields[i].FieldName) <> nil then
          begin
            try
              if not MemTable.Fields[i].IsNull then
              begin
                sValor := QuotedStr(MemTable.Fields[i].AsString);
                if (Fields[i].DataType = ftString) or (Fields[i].DataType = ftWideString) then
                  sValor := QuotedStr(MemTable.Fields[i].AsString);
                if (Fields[i].DataType = ftInteger) or (Fields[i].DataType = ftSmallint) then
                  sValor := MemTable.Fields[i].AsString;
                if (Fields[i].DataType = ftBCD) or (Fields[i].DataType = ftFloat) then
                  sValor := FloattoStrDec(MemTable.Fields[i].AsFloat);
                if (Fields[i].DataType = ftDate) or (Fields[i].DataType = ftDateTime) then
                  sValor := DateTimeToStrISO(MemTable.Fields[i].AsDateTime);
                if (Fields[i].DataType = ftBoolean) then
                  if MemTable.Fields[i].AsBoolean then
                    sValor := '1'
                  else
                    sValor := '0';
                sValor := StringReplace(sValor, #13, '', [rfReplaceAll]);
                sValor := StringReplace(sValor, #10, '', [rfReplaceAll]);
                sValor := StringReplace(sValor, #9, '', [rfReplaceAll]);
              end
              else
                sValor := 'NULL';
              if sCampos <> '' then
                sCampos := sCampos + ', ';
              if sValores <> '' then
                sValores := sValores + ', ';
              sValores := sValores + ' ' + sValor;
              // Comparacion para Pianezzola (Matias)
              if AnsiCompareStr(ansiLowerCase(MemTable.Fields[i].FieldName), 'usuario_creacion') = 0  then
                if sTabla = 'Clientes' then
                  sCampos := sCampos + ' "' + 'Usuario_Creacion' + '"'
                else
                  sCampos := sCampos + ' "' + 'usuario_creacion' + '"'
              else if AnsiCompareStr(ansiLowerCase(MemTable.Fields[i].FieldName), 'usuario_modificacion') = 0  then
                if sTabla = 'Clientes' then
                  sCampos := sCampos + ' "' + 'Usuario_Modificacion' + '"'
                else
                  sCampos := sCampos + ' "' + 'usuario_modificacion' + '"'
              else if AnsiCompareStr(ansiLowerCase(MemTable.Fields[i].FieldName), 'fecha_creacion') = 0  then
                if sTabla = 'Clientes' then
                  sCampos := sCampos + ' "' + 'Fecha_Creacion' + '"'
                else
                  sCampos := sCampos + ' "' + 'fecha_creacion' + '"'
              else if AnsiCompareStr(ansiLowerCase(MemTable.Fields[i].FieldName), 'fecha_modificacion') = 0  then
                if sTabla = 'Clientes' then
                  sCampos := sCampos + ' "' + 'Fecha_Modificacion' + '"'
                else
                  sCampos := sCampos + ' "' + 'fecha_modificacion' + '"'
              else
                sCampos := sCampos + ' "' + fdqryRemoto.FindField(MemTable.Fields[i].FieldName).FieldName + '"';
            except
              on e: exception do
              begin
                mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Error al generar Insert registro remoto ' + sTabla + ' - campo ' + Fields[i].FieldName + ': ' + e.Message);
              end;
            end;

          end
          else
          begin

          end;
        end;
        if (MemTable.FindField('idEmpresa') = nil) then
        begin
          if sCampos <> '' then
            sCampos := sCampos + ', ';
          if sValores <> '' then
            sValores := sValores + ', ';
          sValores := sValores + ' ' + IntToStr(gidEmpresa);
          sCampos := sCampos + ' "idEmpresa"';
        end;
        sQuery := sQuery + '(' + sCampos + ') VALUES (' + sValores + ')';
        fdcmnd.CommandText.Text := sQuery;
        sQuery := StringReplace(sQuery, #13, '', [rfReplaceAll]);
        sQuery := StringReplace(sQuery, #10, '', [rfReplaceAll]);
        sQuery := StringReplace(sQuery, #9, '', [rfReplaceAll]);
        mmoSentencias.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': ' + sQuery + ';');
        try
          fdcmnd.Execute;
          result := true;
        except
          on e: exception do
          begin
            mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Error al Insertar registro remoto ' + sTabla + ': ' + e.Message);
          end;
        end;

      end;
    except
      on e: exception do
      begin
        Result := false;
        sErrMsg := e.Message;
             //DebugLog(LocalPOS.Usuario, 'ActualizarRegistroLocal/'+sTabla+'/'+sCampoClave, 'ERROR ' + sErrMsg);
             //RegistrarDebug('No se puede Actualizar registro local error:'+sErrMsg);
      end;

    end;
  end;
end;

function TfrmMain.ActualizarRegistroRemoto(sTabla: string; MemTable: TADOQuery; var sErrMsg: string): boolean;
var
  sQuery, sValor, sValorClave, sCampos: string;
  i: integer;
begin
  if not MemTable.IsEmpty then
  begin
      //registro
    try //except
      with MemTable do
      begin
        sCampos := '';
        sValorClave := '';
        sQuery := 'UPDATE "' + sTabla + '" SET ';
        for i := 0 to Fields.Count - 1 do
        begin
          if fdqryRemoto.FindField(MemTable.Fields[i].FieldName) <> nil then
          begin
            if not MemTable.Fields[i].IsNull then
            begin
              sValor := QuotedStr(MemTable.Fields[i].AsString);
              if (Fields[i].DataType = ftString) or (Fields[i].DataType = ftWideString) then
                sValor := QuotedStr(MemTable.Fields[i].AsString);
              if (Fields[i].DataType = ftInteger) or (Fields[i].DataType = ftSmallint) then
                sValor := MemTable.Fields[i].AsString;
              if (Fields[i].DataType = ftBCD) or (Fields[i].DataType = ftFloat) then
                sValor := FloattoStrDec(MemTable.Fields[i].AsFloat);
              if (Fields[i].DataType = ftDate) or (Fields[i].DataType = ftDateTime) then
                sValor := DateTimeToStrISO(MemTable.Fields[i].AsDateTime);
              if (Fields[i].DataType = ftBoolean) then
                if MemTable.Fields[i].AsBoolean then
                  sValor := '1'
                else
                  sValor := '0';
            end
            else
              sValor := 'NULL';

            if (MemTable.Fields[i].FieldName) <> 'id' then
            begin
              if sCampos <> '' then
                sCampos := sCampos + ', ';
              // Comparacion para Pianezzola (Matias)
              if AnsiCompareStr(ansiLowerCase(MemTable.Fields[i].FieldName), 'usuario_creacion') = 0  then
                if sTabla = 'Clientes' then
                  sCampos := sCampos + ' "' + 'Usuario_Creacion' + '"=' + sValor
                else
                  sCampos := sCampos + ' "' + 'usuario_creacion' + '"=' + sValor
              else if AnsiCompareStr(ansiLowerCase(MemTable.Fields[i].FieldName), 'usuario_modificacion') = 0  then
                if sTabla = 'Clientes' then
                  sCampos := sCampos + ' "' + 'Usuario_Modificacion' + '"=' + sValor
                else
                  sCampos := sCampos + ' "' + 'usuario_modificacion' + '"=' + sValor
              else if AnsiCompareStr(ansiLowerCase(MemTable.Fields[i].FieldName), 'fecha_creacion') = 0  then
                if sTabla = 'Clientes' then
                  sCampos := sCampos + ' "' + 'Fecha_Creacion' + '"=' + sValor
                else
                  sCampos := sCampos + ' "' + 'fecha_creacion' + '"=' + sValor
              else if AnsiCompareStr(ansiLowerCase(MemTable.Fields[i].FieldName), 'fecha_modificacion') = 0  then
                if sTabla = 'Clientes' then
                  sCampos := sCampos + ' "' + 'Fecha_Modificacion' + '"=' + sValor
                else
                  sCampos := sCampos + ' "' + 'fecha_modificacion' + '"=' + sValor
              else
              sCampos := sCampos + ' "' + MemTable.Fields[i].FieldName + '"=' + sValor;
            end
            else
            begin
              sValorClave := ' "' + fdqryRemoto.FindField(MemTable.Fields[i].FieldName).FieldName + '"=' + sValor;
            end;
          end;
        end;
        sQuery := sQuery + sCampos + ' WHERE ' + sValorClave;
        fdcmnd.CommandText.text := sQuery;
        sQuery := StringReplace(sQuery, #13, '', [rfReplaceAll]);
        sQuery := StringReplace(sQuery, #10, '', [rfReplaceAll]);
        sQuery := StringReplace(sQuery, #9, '', [rfReplaceAll]);
        mmoSentencias.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': ' + sQuery + ';');
        try
          fdcmnd.Execute;
          result := true;
        except
          on e: exception do
          begin
            mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Error al Actualizar registro remoto ' + sTabla + ': ' + e.Message);
          end;
        end;
               //if not EjecutarQuery(sTabla,sQuery,sErrMsg) then
               //begin
               //   DebugLog(LocalPOS.Usuario, 'ActualizarRegistroLocal/'+sTabla+'/'+sCampoClave, 'ERROR ' + sErrMsg);
               //   Raise exception.Create(sErrMsg);
               //end;
        result := true;
      end;
    except
      on e: exception do
      begin
        Result := false;
        sErrMsg := e.Message;
             //DebugLog(LocalPOS.Usuario, 'ActualizarRegistroLocal/'+sTabla+'/'+sCampoClave, 'ERROR ' + sErrMsg);
             //RegistrarDebug('No se puede Actualizar registro local error:'+sErrMsg);
      end;

    end;
  end;
end;


function TfrmMain.DescargarTabla(sTabla:String): boolean;
var
  sQueryLocal, SqueryRemoto, sKey, sErrMsg, sCampo, sID: string;
  pg: TProgressBar;
  guid: TGUID;
  bAlta, bFueModificado: Boolean;
  aValorIndices: Variant;
  i, iAlta, iModificados: integer;
begin
//  if ((sTabla = 'Pedidos') or (sTabla = 'PedidosItems') or (sTabla = 'PedidosItemsDescuentos')) then
//    sQueryLocal := 'SELECT * FROM ' + sTabla + ' WHERE InsertedOn IS NOT NULL ' +
//                    'AND InsertedOn > getdate()-30'
//  else
  sQueryLocal := 'SELECT * FROM ' + sTabla;
  sQueryRemoto :=  'SELECT *,replace(id::varchar(36),''-'','''') as "__id" FROM ' + ' "' + sTabla + '" WHERE "idEmpresa"=' + IntToStr(gidEmpresa) + ' AND "InsertedOn" > (NOW() + interval ''-30 DAY'')';
  try
    try
      tblLocal.Close;
      tblLocal.Filter := '';
      tblLocal.Filtered := False;
      fdqryRemoto.Close;
      fdqryRemoto.sql.text := sQueryRemoto;
      fdqryRemoto.Open;
      fdqryRemoto.FetchAll;
         //kbmMemTable.LoadFromDataSet(fdqryRemoto,[mtcpoStructure]);
         //kbmMemTable.IndexDefs.Add('__id','__id',[]);
      fdqryRemoto.First;
      pg := TProgressBar(FindComponent('pbTabla'));

      if Assigned(pg) then
      begin
        pg.Max := fdqryRemoto.RecordCount;
        pg.Step := 1;
        pg.Position := 0;
      end;
      while not fdqryRemoto.Eof do
      begin
        for i := 0 to fdqryRemoto.Fields.Count - 1 do
        begin
          sCampo := fdqryRemoto.Fields[i].FieldName;
          if sCampo = 'id' then
          begin
            sID := ReplaceText(fdqryRemoto.FieldByName(sCampo).AsString, '{', '');
            sID := ReplaceText(sID, '}', '');
            guid := fdqryRemoto.FieldByName(sCampo).AsGuid;
          end;
        end;

        tblLocal.SQL.Text := sQueryLocal+' WHERE id='+QuotedStr(sID);
        tblLocal.Open;
        bAlta := tblLocal.RecordCount=0;
        if bAlta then
        begin
                //agregar registro online
          if not AgregarRegistroLocal(sTabla, fdqryRemoto, sErrMsg) then
          begin
            mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Error al insertar ' + sTabla + ': ' + sErrMsg);
            if not fdConnectionRemoto.Connected then
              fdConnectionRemoto.Connected := true

          end
          else
            Inc(iAlta);
        end;
        if Assigned(pg) then
          pg.StepIt;
        Application.ProcessMessages;
        fdqryRemoto.Next;
        if (Assigned(pg)) and (Odd(pg.Position)) then
          Application.ProcessMessages
        else if (iAlta + iModificados) mod 10 = 0 then
          Application.ProcessMessages;
      end;
      Result := true;
    except
      on e: Exception do
      begin

        mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Error al descargar ' + sTabla + ': ' + e.Message);

      end;

    end;
  finally
    tblLocal.Close;
    fdqryRemoto.Close;
    if (iAlta > 0) or (iModificados > 0) then
      mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Se dieron de alta ' + IntToStr(iAlta) + ' registros y/o se modificaron ' + IntToStr(iModificados) + ' registros en la tabla ' + sTabla);
  end;

end;

function TfrmMain.Sincronizar(sTabla: string): boolean;
var
  sQueryLocal, SqueryRemoto, sKey, sErrMsg, sCampo, sID: string;
  pg: TProgressBar;
  guid: TGUID;
  bAlta, bFueModificado: Boolean;
  aValorIndices: Variant;
  i, iAlta, iModificados: integer;
  sDesde:string;
  sDsd: string;
  sIdDeposito: string;
  J: Integer;
  cierre: string;
  sTimestampLectura: String;
  huboError: boolean;
  fDesde: string;
begin
  Result := False;
  huboError := False;
  iAlta := 0;
  iModificados := 0;
  sTimestampLectura :=  QuotedStr(FormatDateTime('yyyy-mm-dd', Now) + 'T' + FormatDateTime('hh:nn:ss.zzz', Now));
  fDesde := QuotedStr(FormatDateTime('yyyy-mm-dd', dteDesde.Date) + 'T' + FormatDateTime('hh:nn:00.zzz', tmDesde.Time));
  if chkConCambios.Checked and (not ExisteModelo(sTabla)) then
  begin
    mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': WARNING, no existe modelo de tabla ' + sTabla);
    Result := True;
    exit;
  end;
  if LowerCase(sTabla) = 'itemslista' then
  begin
    if chkConCambios.Checked then
       sDesde:=' AND ( LL.InsertedOn>=(SELECT min(UltimaModificacionDML) FROM Modelos where idTabla='+QuotedStr('Itemslista')+
                 ') OR LL.UpdatedOn>=(SELECT min(UltimaModificacionDML) FROM Modelos where idTabla='+QuotedStr('Itemslista')+' )) ' +
                 'and (LL.InsertedOn is not null or LL.UpdatedOn is not null)'
    else if chkDesdeFecha.Checked then
       sDesde:=' AND ( LL.InsertedOn>=' + fDesde + ' OR LL.UpdatedOn>='+fDesde+' ) ' +
                 'AND (LL.InsertedOn is not null or LL.UpdatedOn is not null)'
    else
       sDesde:='  ';
    sQueryLocal := 'SELECT  id,' + #13#10 +
                              'idLista ,' + #13#10 +
                              'idDeposito ,' + #13#10 +
                              'idProducto ,' + #13#10 +
                              'idUnidad ,' + #13#10 +
                              'ROUND(MAX(RPRECIO), 3) rPrecio ,' + #13#10 +
                              'fVigencia ,' + #13#10 +
                              'CONVERT (VARCHAR(8), fVigencia, 108) hVigencia' + #13#10 +
                      'FROM    ( SELECT    *' + #13#10 +
                                'FROM      ( ( SELECT  IL.id,' + #13#10 +
                                                  'L.idLista ,' + #13#10 +
                                                      'IL.idDeposito ,' + #13#10 +
                                                      'IL.idProducto ,' + #13#10 +
                                                      'IL.idUnidad ,' + #13#10 +
                                                      '( ( IL.rPrecio / MB.rCambio ) * M.rCambio' + #13#10 +
                                                        '* L.rPorcentaje ) AS rPrecio ,' + #13#10 +
                                                      'IL.fVigencia ,' + #13#10 +
                                                      'CONVERT (VARCHAR(8), fVigencia, 108) hVigencia' + #13#10 +
                                              'FROM    ItemsLista IL ,' + #13#10 +
                                                      'Listas L ,' + #13#10 +
                                                      'Listas LB ,' + #13#10 +
                                                      'Monedas M ,' + #13#10 +
                                                      'Monedas MB ,' + #13#10 +
                                                      'Productos P' + #13#10 +
                                              'WHERE   L.idListaBase = LB.idLista' + #13#10 +
                                                      'AND IL.idLista = L.idListaBase' + #13#10 +
                                                      'AND L.idMoneda = M.idMoneda' + #13#10 +
                                                      'AND LB.idMoneda = MB.idMoneda' + #13#10 +
                                                      'AND P.idProducto = IL.idProducto' + #13#10 +
                                                      'AND ISNULL(IL.rPrecio, 0) <> 0' + #13#10 +
                                                      'AND L.idListaBase IS NULL' + #13#10 +
                                                      'AND IL.fVigencia>=getdate()' + #13#10 +
                                                      'AND ISNULL(L.bActivo, 1) = 1    )' + #13#10 +
                                            'UNION ALL' + #13#10 +
                                            '( SELECT  LL.id,' + #13#10 +
                                                      'LL.idLista ,' + #13#10 +
                                                      'LL.idDeposito ,' + #13#10 +
                                                      'LL.idProducto ,' + #13#10 +
                                                      'LL.idUnidad ,' + #13#10 +
                                                      'ROUND(MAX(LL.rPrecioVenta), 3) AS rPrecio ,' + #13#10 +
                                                      'LL.fVigencia ,' + #13#10 +
                                                      'CONVERT (VARCHAR(8), fVigencia, 108) hVigencia' + #13#10 +
                                              'FROM    Listas L' + #13#10 +
                                                      'INNER JOIN LotesLista LL ON ( LL.idLista = L.idLista' + #13#10 +
                                                                                    'AND ( LL.fVigencia IN (' + #13#10 +
                                                                                    'SELECT TOP ( 1 )' + #13#10 +
                                                                                    'fVigencia' + #13#10 +
                                                                                    'FROM' + #13#10 +
                                                                                    'LotesLista ll2' + #13#10 +
                                                                                    'WHERE' + #13#10 +
                                                                                    'll2.fVigencia <= GETDATE()' + #13#10 +
                                                                                    'AND ll2.idLista = LL.idLista' + #13#10 +
                                                                                    'AND ll2.idLote = LL.idLote' + #13#10 +
                                                                                    'AND ll2.idDeposito = LL.idDeposito' + #13#10 +
                                                                                    'AND ll2.idUnidad = LL.idUnidad' + #13#10 +
                                                                                    'AND ll2.idProducto = LL.idProducto' + #13#10 +
                                                                                    'ORDER BY ll2.fVigencia DESC )' + #13#10 +
                                                                                    'OR LL.fVigencia IN (' + #13#10 +
                                                                                    'SELECT' + #13#10 +
                                                                                    'fVigencia' + #13#10 +
                                                                                    'FROM' + #13#10 +
                                                                                    'LotesLista ll2' + #13#10 +
                                                                                    'WHERE' + #13#10 +
                                                                                    'll2.fVigencia > GETDATE()' + #13#10 +
                                                                                    'AND ll2.idLista = LL.idLista' + #13#10 +
                                                                                    'AND ll2.idLote = LL.idLote' + #13#10 +
                                                                                    'AND ll2.idDeposito = LL.idDeposito' + #13#10 +
                                                                                    'AND ll2.idUnidad = LL.idUnidad' + #13#10 +
                                                                                    'AND ll2.idProducto = LL.idProducto )' + #13#10 +
                                                                                    ')' + #13#10 +
                                                                                  ')' + #13#10 +
                                                      'INNER JOIN Productos P ON ( LL.idProducto = P.idProducto )' + #13#10 +
                                              'WHERE   ISNULL(L.bActivo, 1) = 1' + #13#10 +
                                                      'AND LL.rPrecioVenta <> 0' + #13#10 +
                                                      'AND P.bActivo = 1' + #13#10 +
                                                      sDesde +
                                              'GROUP BY LL.id,' + #13#10 +
                                          'LL.idProducto ,' + #13#10 +
                                                      'LL.idUnidad ,' + #13#10 +
                                                      'LL.idLista ,' + #13#10 +
                                                      'LL.idDeposito ,' + #13#10 +
                                                      'LL.fVigencia' + #13#10 +
                                            ')' + #13#10 +
                                          ') AS Precios' + #13#10 +
                              ') AS ITEMSLISTA' + #13#10 +
                      'GROUP BY ID,' + #13#10 +
                              'IDPRODUCTO ,' + #13#10 +
                              'IDUNIDAD ,' + #13#10 +
                              'IDLISTA ,' + #13#10 +
                              'IDDEPOSITO ,' + #13#10 +
                              'FVIGENCIA';
  end
  else
  if LowerCase(sTabla) = 'impxproducto' then
  Begin
    if chkConCambios.Checked then
       sDesde:=' AND ( impxProducto.InsertedOn>=(SELECT min(UltimaModificacionDML) FROM Modelos where idTabla='+QuotedStr('Impxproducto')+
                 ') OR impxProducto.UpdatedOn>=(SELECT min(UltimaModificacionDML) FROM Modelos where idTabla='+QuotedStr('Impxproducto')+' )) ' +
                 'and (impxProducto.InsertedOn is not null or impxProducto.UpdatedOn is not null)'
    else if chkDesdeFecha.Checked then
       sDesde:=' AND ( impxProducto.InsertedOn>=' + fDesde + ' OR impxProducto.UpdatedOn>='+fDesde+' ) ' +
                 'AND (impxProducto.InsertedOn is not null or impxProducto.UpdatedOn is not null)'
    else
       sDesde:='  ';
     sQueryLocal:='(' + #13#10 +
                  '	SELECT *' + #13#10 +
                  '	FROM  impxProducto' + #13#10 +
                  '	WHERE fVigencia IN (' + #13#10 +
                  '	                      (SELECT TOP (1) fVigencia' + #13#10 +
                  '	                       FROM  impxProducto ll2' + #13#10 +
                  '	                       WHERE ll2.fVigencia <= GETDATE()' + #13#10 +
                  '	                         AND ll2.idImpuesto =  impxProducto.idImpuesto' + #13#10 +
                  '	                         AND ll2.idProducto =  impxProducto.idProducto' + #13#10 +
                  '	                       ORDER BY ll2.fVigencia DESC)'+' )'+ sDesde + #13#10 +
                  ')' + #13#10 +
                  'UNION ALL' + #13#10 +
                  '(' + #13#10 +
                  '	SELECT *' + #13#10 +
                  '	FROM  impxProducto' + #13#10 +
                  '	WHERE fVigencia >= getdate()' + #13#10 +
                  sDesde+
                  ')' ;
  End
  else
  if LowerCase(sTabla) = 'impxcliente' then
  Begin
    if chkConCambios.Checked then
       sDesde:=' AND ( impxcliente.InsertedOn>=(SELECT min(UltimaModificacionDML) FROM Modelos where idTabla='+QuotedStr(sTabla)+
             ') OR impxcliente.UpdatedOn>=(SELECT min(UltimaModificacionDML) FROM Modelos where idTabla='+QuotedStr(sTabla)+' )) ' +
             'and (impxcliente.InsertedOn is not null or impxcliente.UpdatedOn is not null)'
    else if chkDesdeFecha.Checked then
       sDesde:=' AND ( impxcliente.InsertedOn>=' + fDesde + ' OR impxcliente.UpdatedOn>='+fDesde+' ) ' +
                 'AND (impxcliente.InsertedOn is not null or impxcliente.UpdatedOn is not null)'
    else
       sDesde:='  ';
   sQueryLocal:='(' + #13#10 +
              '	SELECT *' + #13#10 +
              '	FROM impxcliente' + #13#10 +
              '	WHERE fVigencia IN (' + #13#10 +
              '	                      (SELECT TOP (1) fVigencia' + #13#10 +
              '	                       FROM impxcliente ll2' + #13#10 +
              '	                       WHERE ll2.fVigencia <= GETDATE()' + #13#10 +
              '	                         AND ll2.idImpuesto = impxcliente.idImpuesto' + #13#10 +
              '	                         AND ll2.idCliente = impxcliente.idCliente' + #13#10 +
              '	                       ORDER BY ll2.fVigencia DESC))' + sDesde + #13#10 +
              ')' + #13#10 +
              'UNION ALL' + #13#10 +
              '(' + #13#10 +
              '	SELECT *' + #13#10 +
              '	FROM impxcliente' + #13#10 +
              '	WHERE fVigencia >= getdate()' + #13#10 +  sDesde +
              ')';

  end
  else
  if LowerCase(sTabla) = 'stock' then
  begin
    if chkConCambios.Checked then
      sDesde:=' WHERE ( InsertedOn>=(SELECT min(UltimaModificacionDML) FROM Modelos where idTabla='+QuotedStr('Stock')+
                 ') OR UpdatedOn>=(SELECT min(UltimaModificacionDML) FROM Modelos where idTabla='+QuotedStr('Stock')+' )) ' +
                 'and (InsertedOn is not null or UpdatedOn is not null)'
    else if chkDesdeFecha.Checked then
       sDesde:=' WHERE ( InsertedOn>=' + fDesde + ' OR UpdatedOn>='+fDesde+' ) ' +
                 'AND (InsertedOn is not null or UpdatedOn is not null)'
    else
      sDesde:=' ';
    if gidEmpresa = 21 then
      if sDesde <> ' ' then
        sIdDeposito := ' and idDeposito in (10, 50, 89, 110) '
      else
        sIdDeposito := ' WHERE idDeposito in (10, 50, 89, 110) '
    else
      sIdDeposito := ' ';
    sQueryLocal :=  'SELECT * FROM Stock ' + sDesde + sIdDeposito
  end
  else
  if LowerCase(sTabla) = 'objetivosporvendedor' then
  begin
    if chkConCambios.Checked then
      sDesde:=' WHERE ( InsertedOn>=(SELECT min(UltimaModificacionDML) FROM Modelos where idTabla='+QuotedStr('ObjetivosPorVendedor')+
                 ') OR UpdatedOn>=(SELECT min(UltimaModificacionDML) FROM Modelos where idTabla='+QuotedStr('ObjetivosPorVendedor')+' )) ' +
                 'and (InsertedOn is not null or UpdatedOn is not null)'
    else if chkDesdeFecha.Checked then
       sDesde:=' WHERE ( InsertedOn>=' + fDesde + ' OR UpdatedOn>='+fDesde+' ) ' +
                 'AND (InsertedOn is not null or UpdatedOn is not null)'
    else
      sDesde:=' ';
    sQueryLocal :=  'SELECT * FROM ObjetivosPorVendedor ' + sDesde;
  end
  else
  if LowerCase(sTabla) = 'objetivosporvendedordetalle' then
  begin
    if chkConCambios.Checked then
      sDesde:=' WHERE ( OVD.InsertedOn>=(SELECT min(UltimaModificacionDML) FROM Modelos where idTabla='+QuotedStr('ObjetivosPorVendedorDetalle')+
                 ') OR OVD.UpdatedOn>=(SELECT min(UltimaModificacionDML) FROM Modelos where idTabla='+QuotedStr('ObjetivosPorVendedorDetalle')+' )) ' +
                 'and (OVD.InsertedOn is not null or OVD.UpdatedOn is not null)'
    else if chkDesdeFecha.Checked then
       sDesde:=' WHERE ( OVD.InsertedOn>=' + fDesde + ' OR OVD.UpdatedOn>='+fDesde+' ) ' +
                 'AND (OVD.InsertedOn is not null or OVD.UpdatedOn is not null)'
    else
      sDesde:=' ';
    sQueryLocal :=  'SELECT * FROM ObjetivosPorVendedorDetalle OVD, ObjetivosPorVendedor OV ' + sDesde +
                    ' and OVD.idObjetivo=OV.id ' +
                    ' and OV.fVigDesde<GETDATE() and OV.fVigHasta>GETDATE()';
  end
  else
  if LowerCase(sTabla) = 'doccuenta' then
    sQueryLocal:= 'EXEC SP_Export_DocCuenta'
  else
  begin
    if chkConCambios.Checked then
           sDesde:=' WHERE ( '+sTabla+'.InsertedOn>=(SELECT min(UltimaModificacionDML) FROM Modelos where idTabla='+QuotedStr(sTabla)+
                 ') OR '+sTabla+'.UpdatedOn>=(SELECT min(UltimaModificacionDML) FROM Modelos where idTabla='+QuotedStr(sTabla)+' )) ' +
                 'and ('+sTabla+'.InsertedOn is not null or '+sTabla+'.UpdatedOn is not null)'
    else if chkDesdeFecha.Checked then
       sDesde:=' WHERE ( '+sTabla+'.InsertedOn>=' + fDesde + ' OR '+sTabla+'.UpdatedOn>='+fDesde+' ) ' +
                 'AND ('+sTabla+'.InsertedOn is not null or '+sTabla+'.UpdatedOn is not null)'
    else
       sDesde:='  ';
    sQueryLocal := 'SELECT * FROM ' + sTabla + sDesde ;
  end;

  sQueryRemoto :=  'SELECT *,replace(id::varchar(36),''-'','''') as "__id" FROM ' + ' "' + sTabla + '" WHERE "idEmpresa"=' + IntToStr(gidEmpresa);

  try
    try
      tblLocal.Close;
      tblLocal.Filter := '';
      tblLocal.Filtered := False;
      tblLocal.SQL.Text := sQueryLocal;
      tblLocal.Open;
      tblLocal.First;

      mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Iniciando sincronizacion de tabla ' + sTabla +
                                      ' (' + tblLocal.RecordCount.ToString + ')');

      //////////////////////////////////////////////////////////////////////////////
      // Armo query remoto para tablas grandes    ///////////////////////////////////////
      //////////////////////////////////////////////////////////////////////////////
      if (tblLocal.RecordCount < 1000) and ((LowerCase(sTabla) = 'descxcliente') or (LowerCase(sTabla) = 'impxcliente')
          or (LowerCase(sTabla) = 'descxproducto') or (LowerCase(sTabla) = 'impxproducto')
          or (LowerCase(sTabla) = 'clientes') or (LowerCase(sTabla) = 'productos')
          or (LowerCase(sTabla) = 'doccuenta') or (LowerCase(sTabla) = 'productosxvendedor')
          or (LowerCase(sTabla) = 'stock') or (LowerCase(sTabla) = 'itemslista')) then
      begin
        sQueryRemoto := sQueryRemoto + ' and id in (';
        J := 0;
        while not tblLocal.Eof do
        begin
          if J+1 >= tblLocal.RecordCount then
            cierre := ''
          else
            cierre := ', ';
          sQueryRemoto := sQueryRemoto + quotedStr(ReplaceText(ReplaceText(tblLocal.FieldByName('id').AsString, '{', ''), '}', '')) + cierre;
          J := J+1;
          tblLocal.Next;
        end;
        sQueryRemoto := sQueryRemoto + ')';
      end;

      if not tblLocal.IsEmpty then
      begin
          fdqryRemoto.Close;
          fdqryRemoto.sql.text := sQueryRemoto;
          fdqryRemoto.Open;
          fdqryRemoto.FetchAll;
      end;

      tblLocal.First;

      pg := TProgressBar(FindComponent('pbTabla'));
      if Assigned(pg) then
      begin
        pg.Max := tblLocal.RecordCount;
        pg.Step := 1;
        pg.Position := 0;
      end;
      while not tblLocal.Eof do
      begin
        for i := 0 to tblLocal.Fields.Count - 1 do
        begin
          sCampo := tblLocal.Fields[i].FieldName;
          if sCampo = 'id' then
          begin
                   ///sID := GUIDToString(tblLocal.FieldByName(sCampo).AsGuid);
            sID := ReplaceText(tblLocal.FieldByName(sCampo).AsString, '{', '');
            sID := ReplaceText(sID, '}', '');
            sID := ReplaceText(sID, '-', '');
            guid := tblLocal.FieldByName(sCampo).AsGuid;
          end;
        end;
             //aca tengo el registro local y genero el update
             //ShowMessage(IntToStr(kbmMemTable.RecordCount));
        bAlta := not fdqryRemoto.Locate('__id', sID, [loCaseInsensitive]);
        if bAlta then
        begin
                //agregar registro online
          if not AgregarRegistroRemoto(sTabla, tblLocal, sErrMsg) then
          begin
//            huboError := True;
            mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Error al insertar ' + sTabla + ': ' + sErrMsg);
            if not fdConnectionRemoto.Connected then
              fdConnectionRemoto.Connected := true
          end
          else
            Inc(iAlta);
        end
        else
        begin
          bFueModificado := true; //por defecto
                 //comparar registro
          for i := 0 to tblLocal.Fields.Count - 1 do
          begin
            sCampo := tblLocal.Fields[i].FieldName;
            if (assigned(fdqryRemoto.FindField(sCampo))) and (sCampo <> 'id') then
            begin
              if fdqryRemoto.FieldByName(sCampo).Value <> tblLocal.FieldByName(sCampo).Value then
              begin
                if (fdqryRemoto.FieldByName(sCampo).IsNull = tblLocal.FieldByName(sCampo).IsNull) and (tblLocal.FieldByName(sCampo).IsNull = true) then
                begin
                  bFueModificado := false;
                  Continue;
                end;

                if (fdqryRemoto.FieldByName(sCampo).DataType = ftDateTime) then
                begin
                  if fdqryRemoto.FieldByName(sCampo).AsString <> tblLocal.FieldByName(sCampo).AsString then
                  begin
                    bFueModificado := true;
                    Break;
                  end
                  else
                  begin
                    bFueModificado := false;
                    Continue;
                  end;
                end;

                if (tblLocal.FieldByName(sCampo).DataType = ftBoolean) then
                begin
                  if tblLocal.FieldByName(sCampo).AsBoolean <> (fdqryRemoto.FieldByName(sCampo).AsInteger = 1) then
                  begin
                    bFueModificado := true;
                    Break;
                  end
                  else
                  begin
                    bFueModificado := false;
                    Continue;
                  end;
                end;

                if ((LowerCase(sTabla) = 'descxcliente') or (LowerCase(sTabla) = 'impxcliente')
                    or (LowerCase(sTabla) = 'descxproducto') or (LowerCase(sTabla) = 'impxproducto')
                    or (LowerCase(sTabla) = 'descxlista') or (LowerCase(sTabla) = 'descxpv')
                    or (LowerCase(sTabla) = 'descxvendedor') or (LowerCase(sTabla) = 'unidades')
                    or (LowerCase(sTabla) = 'itemslista')) then
                  if ((lowercase(sCampo)='updatedon') or (lowercase(sCampo)='insertedon') or (lowercase(sCampo)='deletedon')) then
                  begin
                      bFueModificado := true;
                      Break;
                  end;

                if (not (lowercase(sCampo)='updatedon')) and (not (lowercase(sCampo)='insertedon'))
                     and (not (lowercase(sCampo)='deletedon')) then
                  begin
                      bFueModificado := true;
                      Break;
                  end;
              end
              else
                bFueModificado := false;
            end;
          end;
          if bFueModificado then
          begin
                     //modificar registro online
            if not ActualizarRegistroRemoto(sTabla, tblLocal, sErrMsg) then
            begin
//              huboError := True;
              mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Error al actualizar ' + sTabla + ': ' + sErrMsg);
              if not fdConnectionRemoto.Connected then
                fdConnectionRemoto.Connected := true;
            end
            else
              Inc(iModificados);
          end;
        end;
        if Assigned(pg) then
          pg.StepIt;
        Application.ProcessMessages;
        tblLocal.Next;
        if (Assigned(pg)) and (Odd(pg.Position)) then
          Application.ProcessMessages
        else if (iAlta + iModificados) mod 10 = 0 then
          Application.ProcessMessages;
      end;
      //ActualizarModelo(sTabla);
      //acmdLocal.CommandText:='UPDATE Modelos set UltimaModificacionDML=getdate()-(1/24) where upper(idTabla)='+QuotedStr(UpperCase(sTabla));
      // Poner el TimeStamp calculado antes del proceso por si entra un registro durante el proceso
      if not huboError then
      begin
        if (LowerCase(sTabla) = 'doccuenta') and NoCoincideRemotoConLocal(sTimestampLectura) then
          Sincronizar('DocCuenta')
        else
        begin
          acmdLocal.CommandText := 'UPDATE Modelos set UltimaModificacionDML=' + sTimestampLectura + ' where idTabla='+QuotedStr(sTabla);
          acmdLocal.Execute;
        end;
      end;
      Result := true;
    except
      on e: Exception do
      begin

        mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Error al sincronizar ' + sTabla + ': ' + e.Message);

      end;

    end;
  finally
    tblLocal.Close;
    fdqryRemoto.Close;
    if (iAlta > 0) or (iModificados > 0) then
      mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Se dieron de alta ' + IntToStr(iAlta) + ' registros y/o se modificaron ' + IntToStr(iModificados) + ' registros en la tabla ' + sTabla);
  end;
end;

procedure TfrmMain.btnDescargarPedidosClick(Sender: TObject);
var
  i: integer;
  bResultado: boolean;
begin
//  if not tmrAutoExec.enabled then
//    tmrAutoExec.enabled := True;
  if gidEmpresa=-1  then
     raise Exception.Create('No se puede identeficar idEmpresa');
  btnSincronizar.Enabled := false;
  btnSincroSeleccion.Enabled := false;
  btnSincronizarDesdeS.Enabled := false;
  btnDescargarPedidos.Enabled := false;
  try
    LimpiarProgressBar;
    fdConnectionRemoto.Close;
    acnConnectionLocal.Close;
    acnConnectionLocal.ConnectionString:=StringReplace(gStrConn,'%s%s',gsEmpresa, [rfIgnoreCase,rfReplaceAll]);
    try
      fdConnectionRemoto.Connected := True;

    except
      on e: Exception do
      begin
        mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Error al conectar a las BD remota: ' + e.Message);
      end;
    end;
    try
      ConectarLocal;
      i := lstTablas.ItemIndex;
      if i = -1 then
        i := 0;

      mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Iniciando sincronizacion de tabla Pedidos');

      bResultado := DescargarTabla('Pedidos');
      bResultado := DescargarTabla('PedidosItems');
      bResultado := DescargarTabla('PedidosItemsDescuentos');

      if bResultado then
        mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Sincronizada tabla Pedidos')
      else
        mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': ERROR al Sincronizar tabla Pedidos')

    except
      on e: Exception do
      begin
        mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Error al conectar a las BD local: ' + e.Message);
      end;
    end;
    btnSincronizar.Enabled := false;
  finally
    btnSincronizar.Enabled := true;
    btnSincroSeleccion.Enabled := true;
    btnSincronizarDesdeS.Enabled := true;
    btnDescargarPedidos.Enabled := true;
  end;
end;

procedure TfrmMain.btnSincronizarClick(Sender: TObject);
var
  i: integer;
  bResultado: boolean;
  saveFile: string;
begin
  if gidEmpresa=-1  then
     raise Exception.Create('No se puede identeficar idEmpresa');
//  tmrAutoExec.Enabled := false;
  btnSincronizar.Enabled := false;
  btnSincroSeleccion.Enabled := false;
  btnSincronizarDesdeS.Enabled := false;
  btnDescargarPedidos.Enabled := False;
  try
    LimpiarProgressBar;
    fdConnectionRemoto.Close;
    acnConnectionLocal.Close;
    acnConnectionLocal.ConnectionString:=gStrConn;
    if mmoSentencias.Text <> '' then
      mmoSentencias.Clear;
    try
      fdConnectionRemoto.Connected := True;

    except
      on e: Exception do
      begin
        mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Error al conectar a las BD remota: ' + e.Message);
//        tmrAutoExec.Enabled := True;
      end;
    end;
    try
      ConectarLocal;
      for i := 0 to lstTablas.Count - 1 do
      begin
        lstTablas.ItemIndex := i;
        pbGeneral.StepIt;
//        mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Iniciando sincronizacion de tabla ' + lstTablas.Items[i]);
        bResultado := Sincronizar(lstTablas.Items[i]);
        if bResultado then
          mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Sincronizada tabla ' + lstTablas.Items[i])
        else
          mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': ERROR al Sincronizar tabla ' + lstTablas.Items[i]);
      end;

    except
      on e: Exception do
      begin
        mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Error al conectar a las BD local: ' + e.Message);
      end;
    end;
    btnSincronizar.Enabled := false;
  finally
    btnSincronizar.Enabled := true;
    btnSincroSeleccion.Enabled := true;
    btnSincronizarDesdeS.Enabled := true;
    tmrAutoExec.Enabled := False;
    if mmoSentencias.Text <> '' then
    begin
      saveFile := 'logs\SincroTodo_' + formatdatetime('dd-mm-yy_hh.nn.ss', now) + '.txt';
      // mmoLog.Lines.AddStrings(mmoSentencias.Lines);
      mmoLog.Lines.SaveToFile(saveFile);
    end;
    if (ParamCount > 0) and (AnsiCompareStr(AnsiUpperCase(ParamStr(1)), 'AUTO') = 0) then
      tmrCerrar.Enabled := true;
  end;
end;

procedure TfrmMain.btnSincronizarDesdeSClick(Sender: TObject);
var
  i: integer;
  bResultado: boolean;
begin
  if gidEmpresa=-1  then
     raise Exception.Create('No se puede identeficar idEmpresa');
  tmrAutoExec.Enabled := false;
  btnSincronizar.Enabled := false;
  btnSincroSeleccion.Enabled := false;
  btnSincronizarDesdeS.Enabled := false;
  btnDescargarPedidos.Enabled := false;
  try
    LimpiarProgressBar;
    fdConnectionRemoto.Close;
    acnConnectionLocal.Close;
    acnConnectionLocal.ConnectionString:=StringReplace(gStrConn,'%s%s',gsEmpresa, [rfIgnoreCase,rfReplaceAll]);
    try
      fdConnectionRemoto.Connected := True;

    except
      on e: Exception do
      begin
        mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Error al conectar a las BD remota: ' + e.Message);
      end;
    end;
    try
      ConectarLocal;
      for i := lstTablas.ItemIndex to lstTablas.Count - 1 do
      begin
        pbGeneral.StepIt;
        lstTablas.ItemIndex:=i;
//        mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Iniciando sincronizacion de tabla ' + lstTablas.Items[i]);
        bResultado := Sincronizar(lstTablas.Items[i]);
        if bResultado then
          mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Sincronizada tabla ' + lstTablas.Items[i])
        else
          mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': ERROR al Sincronizar tabla ' + lstTablas.Items[i])

      end;

    except
      on e: Exception do
      begin
        mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Error al conectar a las BD local: ' + e.Message);
      end;
    end;
    btnSincronizar.Enabled := false;
  finally
    btnSincronizar.Enabled := true;
    btnSincroSeleccion.Enabled := true;
    btnSincronizarDesdeS.Enabled := true;
//    tmrAutoExec.Enabled := true;
  end;
end;

procedure TfrmMain.btnSincroSeleccionClick(Sender: TObject);
var
  i: integer;
  bResultado: boolean;
  tablas: Tlist<String>;
begin
  if gidEmpresa=-1  then
     raise Exception.Create('No se puede identeficar idEmpresa');
  tablas := TList<String>.Create;
  tmrAutoExec.Enabled := false;
  btnSincronizar.Enabled := false;
  btnSincroSeleccion.Enabled := false;
  btnSincronizarDesdeS.Enabled := false;
  btnDescargarPedidos.Enabled := false;

  // Al sincronizar tablas individuales tambien deberia sincronizar sus tablas relacionadas.
  if lstTablas.Items[lstTablas.ItemIndex] = 'Clientes' then
    tablas.AddRange(['Clientes', 'DescXCliente', 'ImpXCliente'])
  else if lstTablas.Items[lstTablas.ItemIndex] = 'Productos' then
    tablas.AddRange(['Productos', 'DescXProducto', 'ImpXProducto'])
  else if lstTablas.Items[lstTablas.ItemIndex] = 'Listas' then
    tablas.AddRange(['Listas', 'ImpXLista', 'DescXLista'])
  else if lstTablas.Items[lstTablas.ItemIndex] = 'Impuestos' then
    tablas.AddRange(['Impuestos', 'ImpXLista', 'ImpXPV', 'ImpXProducto', 'ImpXCliente'])
  else if lstTablas.Items[lstTablas.ItemIndex] = 'Descuentos' then
    tablas.AddRange(['Descuentos', 'DescXLista', 'DescXPV', 'DescXProducto', 'DescXCliente']);


  try
    LimpiarProgressBar;
    fdConnectionRemoto.Close;
    acnConnectionLocal.Close;
    acnConnectionLocal.ConnectionString:=StringReplace(gStrConn,'%s%s',gsEmpresa, [rfIgnoreCase,rfReplaceAll]);
    try
      fdConnectionRemoto.Connected := True;

    except
      on e: Exception do
      begin
        mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Error al conectar a las BD remota: ' + e.Message);
      end;
    end;
    try
      ConectarLocal;
      if tablas.Count > 0 then
      begin
        pbGeneral.Max := tablas.Count;
        for i := 0 to tablas.Count - 1 do
        begin
          lstTablas.ItemIndex := lstTablas.Items.IndexOf(tablas.Items[i]);
          pbGeneral.StepIt;
          bResultado := Sincronizar(tablas.Items[i]);
          if bResultado then
            mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Sincronizada tabla ' + tablas.Items[i])
          else
            mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': ERROR al Sincronizar tabla ' + tablas.Items[i])
        end
      end
      else
      begin
        i := lstTablas.ItemIndex;
        if i = -1 then
          i := 0;
        bResultado := Sincronizar(lstTablas.Items[i]);
        if bResultado then
          mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Sincronizada tabla ' + lstTablas.Items[i])
        else
          mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': ERROR al Sincronizar tabla ' + lstTablas.Items[i])
      end;

    except
      on e: Exception do
      begin
        mmoLog.Lines.Add(FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': Error al conectar a las BD local: ' + e.Message);
      end;
    end;
    btnSincronizar.Enabled := false;
  finally
    btnSincronizar.Enabled := true;
    btnSincroSeleccion.Enabled := true;
    btnSincronizarDesdeS.Enabled := true;
//    tmrAutoExec.Enabled := true;
    tablas.Free;
  end;
end;

procedure TfrmMain.chkDesdeFechaClick(Sender: TObject);
begin
  if chkDesdeFecha.Checked then
    chkConCambios.Checked := false
  else
    chkConCambios.Checked := true;
end;

function TfrmMain.NoCoincideRemotoConLocal(timeStamp: String): boolean;
var
  sQueryLocal, SqueryRemoto: string;
  cantLocal, cantRemota: Integer;
begin
  Result := False;
  sQueryLocal:= 'EXEC SP_Export_DocCuenta';
  sQueryRemoto :=  'SELECT COUNT(*) as cant FROM "DocCuenta" WHERE "idEmpresa"=' + IntToStr(gidEmpresa) +
                   ' and "InsertedOn">=' + QuotedStr(timeStamp) + ' OR "UpdatedOn">=' + QuotedStr(timeStamp);
  tblLocal.Close;
  tblLocal.Filter := '';
  tblLocal.Filtered := False;
  tblLocal.SQL.Text := sQueryLocal;
  tblLocal.Open;
  tblLocal.First;
  cantLocal := tblLocal.RecordCount;
  fdqryRemoto.Close;
  fdqryRemoto.sql.text := sQueryRemoto;
  fdqryRemoto.Open;
  fdqryRemoto.First;
  cantRemota := fdqryRemoto.FieldByName('cant').AsInteger;

  if cantRemota < cantLocal then
  begin
    mmoLog.Lines.Add('No coincide Remoto =>' + IntToStr(cantRemota) + ' con local =>' + IntToStr(cantLocal) + '. Resincronizando...');
    Result := True;
  end;
end;

end.

