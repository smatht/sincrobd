
CREATE PROCEDURE [dbo].[SP_Export_DocCuenta] @iVendedor INT = NULL 
 AS
 BEGIN
  SELECT D.id, D.iCuota, D.idTipoDoc, D.idPV, D.idNumDoc, D.rImpCuota,
         D.rImpPagado, D.rImpSaldo, D.fCancelada, D.fCancelada, d.sEstado,
         D.rTotFactura, D.iCantCuotas, D.idCliente, D.idVendedor, D.iDiasAtraso, D.fVtoCuota,
         V.idMoneda, V.fDocumento, D.InsertedOn, D.UpdatedOn, D.DeletedOn
           FROM DocCuenta D, Ventas V, Clientes C 
  WHERE V.idTipoDoc=D.idTipoDoc AND V.idPV = D.idPV 
  AND V.idNumDoc = D.idNumDoc 
  AND sEstado=0 AND V.idCliente = C.idCliente AND C.bActivo=1
  --AND (C.idVendedor =@iVendedor or C.idVendedor2 = @iVendedor) 
  AND D.idVendedor = ISNULL(@iVendedor,D.idVendedor) --Agregado Emanuel 11-11-2016
  --AND D.fVtoCuota <= getdate() --Comentado por pedido de Mauricio 23-11-2016
  --and D.fVtoCuota>='2013-01-01T00:00:00' --pedido de Pianezzola
  AND (ISNULL(D.InsertedOn,GETDATE()) >=
                              (SELECT min(UltimaModificacionDML)
                               FROM Modelos
                               WHERE  UPPER(idTabla)='DOCCUENTA'
                                 OR D.UpdatedOn>=
                                   (SELECT min(UltimaModificacionDML)
                                    FROM Modelos
                                    WHERE UPPER(idTabla)='DOCCUENTA')))
 END 

