/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : SQL Server
 Source Server Version : 12002269
 Source Host           : .:1433
 Source Catalog        : FidaniCtes
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 12002269
 File Encoding         : 65001

 Date: 10/10/2018 09:02:12
*/


-- ----------------------------
-- Table structure for Modelos
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Modelos]') AND type IN ('U'))
	DROP TABLE [dbo].[Modelos]
GO

CREATE TABLE [dbo].[Modelos] (
  [idTabla] varchar(50) COLLATE Modern_Spanish_CI_AS  NOT NULL,
  [iTipoSincro] int DEFAULT ((1)) NULL,
  [iOrderSincro] int DEFAULT ((1)) NULL,
  [sDescripcion] varchar(100) COLLATE Modern_Spanish_CI_AS  NULL,
  [bVisible] varchar(1) COLLATE Modern_Spanish_CI_AS DEFAULT ('T') NULL,
  [UltimaModificacionDML] datetime DEFAULT (getdate()) NULL,
  [UltimaModificacionDDL] datetime DEFAULT (getdate()) NULL,
  [InsertedOn] datetime DEFAULT (getdate()) NULL,
  [UpdatedOn] datetime  NULL,
  [DeletedOn] datetime  NULL
)
GO

ALTER TABLE [dbo].[Modelos] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of Modelos
-- ----------------------------
INSERT INTO [dbo].[Modelos]  VALUES (N'Bancos', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:07.617', N'2018-10-09 16:31:07.617', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Barrios', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:07.650', N'2018-10-09 16:31:07.650', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Canales', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:07.677', N'2018-10-09 16:31:07.677', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Categoriaclientes', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:07.703', N'2018-10-09 16:31:07.703', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Clientes', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:07.733', N'2018-10-09 16:31:07.733', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'ClientesPorEncuesta', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:15.907', N'2018-10-09 16:31:15.907', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Clientesxgfamilia', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:07.760', N'2018-10-09 16:31:07.760', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Condpago', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:07.787', N'2018-10-09 16:31:07.787', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Condventa', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:07.813', N'2018-10-09 16:31:07.813', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Cotizaciones', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:07.843', N'2018-10-09 16:31:07.843', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Depositos', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:07.870', N'2018-10-09 16:31:07.870', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Descuentos', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.013', N'2018-10-09 16:31:08.013', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Descxcliente', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:07.900', N'2018-10-09 16:31:07.900', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Descxlista', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:07.927', N'2018-10-09 16:31:07.927', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Descxproducto', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:07.957', N'2018-10-09 16:31:07.957', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Descxvendedor', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:07.987', N'2018-10-09 16:31:07.987', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Doccuenta', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.333', N'2018-10-09 16:31:08.333', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Empresas', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:07.527', N'2018-10-09 16:31:07.527', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Encuestas', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.360', N'2018-10-09 16:31:08.360', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Familias', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:09.050', N'2018-10-09 16:31:09.050', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Grupofamilia', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:09.073', N'2018-10-09 16:31:09.073', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Gruponumeracion', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.387', N'2018-10-09 16:31:08.387', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Impuestos', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.497', N'2018-10-09 16:31:08.497', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Impxcliente', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.413', N'2018-10-09 16:31:08.413', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Impxlista', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.440', N'2018-10-09 16:31:08.440', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Impxproducto', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.470', N'2018-10-09 16:31:08.470', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Itemslista', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.520', N'2018-10-09 16:31:08.520', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Lineas', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:09.100', N'2018-10-09 16:31:09.100', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Listas', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:09.127', N'2018-10-09 16:31:09.127', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Localidades', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.550', N'2018-10-09 16:31:08.550', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'LotesLista', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:09.180', N'2018-10-09 16:31:09.180', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Modelos', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:07.587', N'2018-10-09 16:31:07.587', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Monedas', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.577', N'2018-10-09 16:31:08.577', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Motivosnocompra', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.603', N'2018-10-09 16:31:08.603', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Numeraciondoc', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.630', N'2018-10-09 16:31:08.630', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'ObjetivosPorVendedor', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:14.747', N'2018-10-09 16:31:14.747', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'ObjetivosPorVendedorDetalle', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:14.773', N'2018-10-09 16:31:14.773', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'OpcionesPorPregunta', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:15.853', N'2018-10-09 16:31:15.853', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Paises', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.660', N'2018-10-09 16:31:08.660', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Pedidos', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.690', N'2018-10-09 16:31:08.690', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Pedidositems', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.717', N'2018-10-09 16:31:08.717', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Pedidositemsdescuentos', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.747', N'2018-10-09 16:31:08.747', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'PreguntasPorEncuesta', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:15.823', N'2018-10-09 16:31:15.823', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Productos', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:09.153', N'2018-10-09 16:31:09.153', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'ProductosXVendedor', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:15.933', N'2018-10-09 16:31:15.933', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Provincias', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.773', N'2018-10-09 16:31:08.773', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'RespuestaPorPregunta', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:15.880', N'2018-10-09 16:31:15.880', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Rutas', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.803', N'2018-10-09 16:31:08.803', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Stock', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.830', N'2018-10-09 16:31:08.830', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Subcanales', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.857', N'2018-10-09 16:31:08.857', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Sucursales', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.883', N'2018-10-09 16:31:08.883', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Tiposdoc', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.910', N'2018-10-09 16:31:08.910', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Tiposlote', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.940', N'2018-10-09 16:31:08.940', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Unidades', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.967', N'2018-10-09 16:31:08.967', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Vendedores', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:08.993', N'2018-10-09 16:31:08.993', NULL, NULL)
GO

INSERT INTO [dbo].[Modelos]  VALUES (N'Zonas', N'1', N'1', NULL, N'T', N'1900-01-01 00:00:00.000', N'2018-10-09 16:31:09.023', N'2018-10-09 16:31:09.023', NULL, NULL)
GO


-- ----------------------------
-- Primary Key structure for table Modelos
-- ----------------------------
ALTER TABLE [dbo].[Modelos] ADD CONSTRAINT [PK__Modelos__716BDE20FB02ED06] PRIMARY KEY CLUSTERED ([idTabla])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

